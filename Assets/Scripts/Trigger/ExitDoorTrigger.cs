using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitDoorTrigger : MonoBehaviour
{
    [SerializeField] private Animator doorAnimator;
    [SerializeField] private float closingDelay;

    private void OnTriggerEnter(Collider other)
    {
        //ball is on the ramp; move down the door
        if (other.gameObject.CompareTag("Ball"))
        {
            SetBallOnRamp(true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        //ball is out of the ramp; start coroutine to move up the door
        if (other.gameObject.CompareTag("Ball"))
        {
            StartCoroutine(WaitForDoorClosing());
        }
    }

    //method to switch between door up or down
    private void SetBallOnRamp(bool ballOnRamp)
    {
        doorAnimator.SetBool("ballOnRamp", ballOnRamp);
    }


    private IEnumerator WaitForDoorClosing()
    {
        //wait for the given secopnds and set animaotr to move up the door
        yield return new WaitForSeconds(closingDelay);
        SetBallOnRamp(false);
    }
}
