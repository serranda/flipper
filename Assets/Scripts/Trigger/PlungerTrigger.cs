using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlungerTrigger : MonoBehaviour
{
    [SerializeField] private string inputName;

    [SerializeField] private float minPower;
    [SerializeField] private float maxPower;
    private float power;

    [SerializeField] private float easeTime;
    private LTDescr plungerDescr;

    [SerializeField] private Animator plungerAnimator;

    private Rigidbody ballRigidbody;
    private bool ballIsReady;

    private void Start()
    {
        //create leantween descr to calculate power value increment according to the easeout value; this will influence also the animation
        plungerDescr = LeanTween.value(minPower, maxPower, easeTime).setOnUpdate(SetPower).setEaseOutCubic();
        ballIsReady = false;
        ResetPlunger();
    }

    //while pressing SPACE increase power until maxPower; when release SPACE ad force to ball rigidbody
    private void FixedUpdate()
    {
        //bal is in contact with the plunger
        if (ballIsReady)
        {
            // The user presses SPACE
            if (Input.GetAxis(inputName) == 1)
            {
                if (power <= maxPower)
                {
                    //resume leantween and starting calculatin the power
                    plungerDescr.resume();
                }
            }
            // The user releases SPACE
            if (Input.GetAxis(inputName) == 0)
            {
                if (ballRigidbody != null)
                {
                    //add force to the ball (first one is a normal force, second line is explosion force; uncomment the one you like to use

                    ballRigidbody.AddForce(power * Vector3.forward);
                    //ballRigidbody.AddExplosionForce(power, transform.position, 1);
                }
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        //if ball enter collider set flag to start use plunger; get ball rigidbody
        if (other.gameObject.CompareTag("Ball") && !ballIsReady)
        {

            //ball can be launched; set flag to tru to enable plunger
            ballIsReady = true; ;

            //set rigidbody in PlungerTrigger class to add force later
            SetBallRigidbody(other.attachedRigidbody);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        //if ball exited from the trigger set flag to disable plunger
        if (other.gameObject.CompareTag("Ball"))
        {
            //ball has been launched; set flag to false to disable plunger
            ballIsReady = false;

            //reset rigidbody in PlungerTrigger class
            SetBallRigidbody(null);

            //reset plunger animation and power
            ResetPlunger();
        }
    }

    //set leantween descr at 0 time and puase it; set power to 0
    public void ResetPlunger()
    {
        plungerDescr.pause();
        plungerDescr.setPassed(0);
        SetPower(0);
    }

    //set power to given value
    private void SetPower(float powerVal)
    {
        power = powerVal;
        SetPlungerAnimator(powerVal);
    }

    //set the animator blend tree relative to the amount of the actual power
    private void SetPlungerAnimator(float powerVal)
    {
        float step = powerVal / maxPower;
        plungerAnimator.SetFloat("Power", step);
    }

    //set the rigidbody of the ball
    public void SetBallRigidbody(Rigidbody rigidbody)
    {
        ballRigidbody = rigidbody;
    }
}
