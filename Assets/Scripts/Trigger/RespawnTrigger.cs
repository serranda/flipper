using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RespawnTrigger : MonoBehaviour
{
    [SerializeField] private Transform ballSpawn;

    private void OnTriggerEnter(Collider other)
    {
        //Ball entered in the respawn area; respawn a new one in the assigned position
        if (other.gameObject.CompareTag("Ball"))
        {
            GameManager.Instance.RespawnBall(ballSpawn);
        }
    }
}
