using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBottomLimiter : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Enemy"))
        {
            GameManager.Instance.GameOver();
        }
    }
}
