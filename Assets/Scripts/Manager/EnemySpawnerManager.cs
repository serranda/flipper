using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class EnemySpawnerManager : MonoBehaviour
{
    [SerializeField] private float startWaveScore;
    [SerializeField] private Transform enemySpawnPoint;
    [SerializeField] private GameObject enemyRowPrefab;
    [SerializeField] private GameObject bossRowPrefab;
    [SerializeField] private int[] waveEnemyIds;

    [SerializeField] private float lateralMovement;
    [SerializeField] private float downMovement;
    [SerializeField] private float movementWaitingSeconds;

    private int _waveCounter;
    [SerializeField] private int maximumSimultaneousRow;
    private int _rowCounter;

    private GameObject _lastRow;

    [SerializeField] private float bossSpawnScore;

    public static EnemySpawnerManager Instance;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        //set wave counter to 0
        _waveCounter = 0;

        //set row counter to 0
        _rowCounter = 0;

        //start coroutine to spawn new enemy waves
        StartCoroutine(NewWave());
    }


    private IEnumerator NewWave()
    {
        yield return new WaitUntil(() => ScoreManager.Instance.GetScore() >= startWaveScore);

        //looping spawning waves until reached the array limits
        for (int i = 0; i < waveEnemyIds.Length; i++)
        {
            //spawn a new enemy row with the related enemyId
            SpawnRow(waveEnemyIds[_waveCounter]);

            //increment wave counter
            _waveCounter++;

            //wait for first movement
            yield return new WaitForSeconds(movementWaitingSeconds);

            //wait until next spawn
            yield return new WaitUntil(() => Math.Abs(_lastRow.transform.position.x - enemySpawnPoint.position.x) < 0.2f);

            //wait if reached the maximum amount of row
            yield return new WaitWhile(() => _rowCounter >= maximumSimultaneousRow);
        }

        if (ScoreManager.Instance.GetScore() > bossSpawnScore)
        {
            SpawnBossRow();
        }

        GameManager.Instance.GameOver();
    }

    private void SpawnRow(int enemyId)
    {
        //instantiate a new row of the specified enemyID 
        GameObject enemyRow = Instantiate(enemyRowPrefab, enemySpawnPoint.transform, false);
        _lastRow = enemyRow;

        //get the enemy row controller and set the initial variable
        EnemyRowController rowController = enemyRow.GetComponent<EnemyRowController>();
        rowController.SetMovementVariables(lateralMovement, downMovement, movementWaitingSeconds);
        rowController.PopulateEnemyRow(enemyId);
        StartCoroutine(rowController.Move());

        //increase rowCounter
        _rowCounter++;
    }

    private void SpawnBossRow()
    {
        //instantiate a new row of the specified enemyID 
        GameObject bossRow = Instantiate(bossRowPrefab, enemySpawnPoint.transform, false);

        //get the enemy row controller and set the initial variable
        BossRowController rowController = bossRow.GetComponent<BossRowController>();
        rowController.SetMovementVariables(lateralMovement, downMovement, movementWaitingSeconds);
        rowController.PopulateBossRow();
        StartCoroutine(rowController.Move());

        //increase rowCounter
        _rowCounter++;
    }

    public void RowDestroyed()
    {
        _rowCounter--;
    }

}
