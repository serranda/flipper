using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private GameObject _ballObject;

    [SerializeField] private int ballLeft;
    [SerializeField] private TextMeshProUGUI ballText;
    [SerializeField] private GameObject ballPrefab;
    [SerializeField] private Transform startBallSpawn;

    [SerializeField] private GameObject gameOverScreen;
    [SerializeField] private GameObject pauseScreen;

    [SerializeField] private TextMeshProUGUI finalScoreText;


    public static GameManager Instance;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        SpawnBall();
        ballText.text = ballLeft.ToString();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SetPauseMenu(!pauseScreen.activeInHierarchy);
        }
    }

    //spawn a new ball in the default position
    public void SpawnBall()
    {
        _ballObject = GameObject.Instantiate(ballPrefab, startBallSpawn.position, Quaternion.identity);
        UpdateBallLeft();
    }

    //spawn a new ball in the given position
    public void SpawnBall(Transform ballSpawn)
    {
        _ballObject = GameObject.Instantiate(ballPrefab, ballSpawn.position, Quaternion.identity);
        UpdateBallLeft();
    }

    //destroy old ball and spawn a new one
    public void RespawnBall(Transform ballSpawn)
    {
        GameObject.Destroy(_ballObject);
        if (ballLeft > 0)
        {
            SpawnBall(ballSpawn);
        }
        else
        {
            GameOver();
        }
    }

    public void GameOver()
    {
        Time.timeScale = 0;
        
        SetGameOverScreen(true);
    }

    private void UpdateBallLeft()
    {
        ballLeft--;
        ballText.text = ballLeft.ToString();
    }

    public void SetGameOverScreen(bool active)
    {
        finalScoreText.text = ScoreManager.Instance.GetScore().ToString();
        gameOverScreen.SetActive(active);
    }

    public void SetPauseMenu(bool active)
    {
        Time.timeScale = 1 - Time.timeScale;

        pauseScreen.SetActive(active);
    }
}
