using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{
    [SerializeField] private float score;
    [SerializeField] private TextMeshProUGUI scoreText;

    public static ScoreManager Instance;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        SetScore(0);
    }

    //Increment score by given points
    public void AddPoints(float points)
    {
        score += points;
        scoreText.text = score.ToString();
    }

    private void SetScore(float newScore)
    {
        score = newScore;
        scoreText.text = score.ToString();
    }

    public float GetScore()
    {
        return score;
    }
}
