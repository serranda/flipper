using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyRowController : MonoBehaviour
{
    private float _lateralMovement;
    private float _downMovement;
    private float _movementWaitingSeconds;

    [SerializeField] private List<GameObject> enemyPrefabs;
    [SerializeField] private List<Transform> enemySpawns;
    [SerializeField] private List<GameObject> enemyGameObjects;

    private int _direction;
    private bool _hasToMoveDown;
 

    private void Start()
    {
        _direction = 1;
        _hasToMoveDown = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        //object has collided with limiter on table; need to change direction
        if (other.gameObject.CompareTag("Limiter"))
        {
            _direction = -_direction;
            _hasToMoveDown = true;            
        }
    }

    public IEnumerator Move()
    {
        //looping to keep moving the row
        while (gameObject.activeInHierarchy)
        {
            //row is at the lateral limit, next movement is down
            if (_hasToMoveDown)
            {
                gameObject.transform.position += _downMovement * Vector3.back;
                _hasToMoveDown = false;
            }
            //move laterally in the specified direction
            else
            {
                gameObject.transform.position += _direction * _lateralMovement * Vector3.right;
            }
            yield return new WaitForSeconds(_movementWaitingSeconds);
        }
    }

    //set the lateral direction (right = 1; left = -1)
    public void SetDirection(int newDirection)
    {
        _direction = newDirection;
    }

    //set the movement variables
    public void SetMovementVariables(float lateral, float down, float wait)
    {
        _lateralMovement = lateral;
        _downMovement = down;
        _movementWaitingSeconds = wait;
    }

    //instantiate in the row a number of specified enemyId
    public void PopulateEnemyRow(int enemyId)
    {
        enemyGameObjects = new List<GameObject>();
        GameObject prefabToInstantiate = enemyPrefabs[enemyId];
        for (int i = 0; i < enemySpawns.Count; i++)
        {
            enemyGameObjects.Add(Instantiate(prefabToInstantiate, enemySpawns[i].transform, false));
            enemyGameObjects[i].GetComponentInChildren<EnemyController>().SetEnemyRowController(gameObject);
        }
    }

    //an enemy in the row has been hit; remove it from the list
    public void EnemyHit(GameObject enemyObject)
    {
        enemyGameObjects.Remove(enemyObject);

        //no more enemy in the row; destroy the row
        if (enemyGameObjects.Count == 0)
        {
            gameObject.SetActive(false);
            EnemySpawnerManager.Instance.RowDestroyed();
        }
    }
}
