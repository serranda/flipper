using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlipperController : MonoBehaviour
{
    [SerializeField] private float restPosition = 0f;
    [SerializeField] private float pressedPosition = 45f;
    [SerializeField] private float hitStrenght = 50000f;
    [SerializeField] private float flipperDamper = 150f;

    private JointSpring _spring;
    [SerializeField] private HingeJoint hinge;

    [SerializeField] private string inputName;

    
    private void Start()
    {
        // Setup the hinge spring
        hinge.useSpring = true;
        _spring = new JointSpring
        {
            spring = hitStrenght,
            damper = flipperDamper
        };
    }

    private void FixedUpdate()
    {
        //player has pressed the input key; set spring to pressed position
        if (Input.GetAxis(inputName) == 1)
        {
            _spring.targetPosition = pressedPosition;
        }
        //player has released the input key; set spring to rest position
        else
        {
            _spring.targetPosition = restPosition;
        }

        //set the spring to the hinge in order to move the joint
        hinge.spring = _spring;
    }
}
