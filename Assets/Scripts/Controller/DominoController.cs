using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DominoController : MonoBehaviour
{
    [SerializeField] private BumperController bumperController;

    private void OnCollisionEnter(Collision collision)
    {
        //ball has collided with the domino
        if (collision.gameObject.CompareTag("Ball"))
        {
            //set the animator bool to start animation
            bumperController.SetBumperAnimator(true);

            //start timer coroutine
            StartCoroutine(bumperController.WaitTimer());
        }
    }
}

