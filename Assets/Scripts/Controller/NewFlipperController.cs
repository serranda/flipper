using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewFlipperController : MonoBehaviour
{
    [SerializeField] private string inputName;

    private ConfigurableJoint joint;
    [SerializeField] private float restAngle;
    [SerializeField] private float hitAngle;

    void Start()
    {
        joint = GetComponent<ConfigurableJoint>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {

        if (Input.GetAxis(inputName) == 1)
        {
            Quaternion hitRotation = Quaternion.AngleAxis(hitAngle, Vector3.up);
            joint.targetRotation = hitRotation;
        }
        else
        {
            Quaternion restRotation = Quaternion.AngleAxis(restAngle, Vector3.up);
            joint.targetRotation = restRotation;
        }
    }
}
