using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BumperController : MonoBehaviour
{
    [SerializeField] private float bumperPower;
    [SerializeField] private float bumperPoints;
    [SerializeField] private Animator bumperAnimator;

    [SerializeField] private float waitSeconds;

    [SerializeField] private bool _enemyOnBumper;

    private void Start()
    {
        _enemyOnBumper = false;
    }

    private void OnCollisionEnter(Collision collision)
    {
        //ball has collided with the bumper
        if (collision.gameObject.CompareTag("Ball"))
        {
            //get ball rigidbody
            Rigidbody ballRigidbody = collision.rigidbody;

            //add force to the ball (first one is a normal force, second line is explosion force; uncomment the one you like to use

            ballRigidbody.AddForce(-1 * bumperPower * collision.contacts[0].normal);
            //ballRigidbody.AddExplosionForce(bumperPower, transform.position, 1);

            //Increment game score
            ScoreManager.Instance.AddPoints(bumperPoints);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        //enemy hit the bumper; disable the bumper
        if (other.gameObject.CompareTag("Enemy"))
        {
            if (!_enemyOnBumper)
            {
                SetBumperAnimator(true);
                //start timer coroutine
                StartCoroutine(WaitTimer());
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        //enemy hit the bumper; disable the bumper
        if (other.gameObject.CompareTag("Enemy"))
        {
            _enemyOnBumper = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Enemy"))
        {
            _enemyOnBumper = false;
        }
    }

    //disable the bumper game object
    public void SetBumperAnimator(bool isUp)
    {
        bumperAnimator.SetBool("hitted", isUp);
    }

    public IEnumerator WaitTimer()
    {
        //wait for the given seconds and then set the animator bool to start animation
        yield return new WaitForSeconds(waitSeconds);
        yield return new WaitUntil(() => _enemyOnBumper == false);

        SetBumperAnimator(false);
    }
}
