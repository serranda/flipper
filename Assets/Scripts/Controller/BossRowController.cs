using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossRowController : EnemyRowController
{
    private GameObject _bossGameObject;
    [SerializeField] private GameObject bossPrefab;
    [SerializeField] private Transform bossSpawn;

    public void PopulateBossRow()
    {
        _bossGameObject = Instantiate(bossPrefab, bossSpawn.transform, false);
        _bossGameObject.GetComponentInChildren<EnemyController>().SetEnemyRowController(gameObject);

    }
}
