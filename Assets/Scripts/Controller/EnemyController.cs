using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    [SerializeField] private EnemyRowController enemyRowController;
    [SerializeField] private GameObject enemyObject;

    private void OnCollisionEnter(Collision collision)
    {
        //ball has collided with the bumper
        if (collision.gameObject.CompareTag("Ball"))
        {
            //inform rowcontroller that enemy has been hit
            enemyRowController.EnemyHit(enemyObject);

            //disable the object
            enemyObject.SetActive(false);
        }
    }

    //set enemy row controller associated at this enemy 
    public void SetEnemyRowController(GameObject row)
    {
        enemyRowController = row.GetComponent<EnemyRowController>();
    }
}
